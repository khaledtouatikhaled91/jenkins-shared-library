#!/usr/bin/env groovy

import com.example.Docker

def call() {
        echo "building the docker image..."
    withCredentials([usernamePassword(credentialsId: 'dockercredentials', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
        sh 'docker build -t khaledkhaled/nana:jma-2.0 .'
        sh "echo $PASS | docker login -u $USER --password-stdin"
        sh 'docker push khaledkhaled/nana:jma-2.0 .'
    }
}
